import 'dart:async';

import 'package:flutter/material.dart';

import '../Model/ForgotPassword/ForgotPasswordRequest.dart';
import '../Model/ForgotPassword/otp_verification/ForgotPasswordOtpRequest.dart';
import '../Model/Login/LoginRequest.dart';
import '../Model/SignUp/SignUpRequest.dart';
import '../Model/SignUp/registration_otp/RegistrationOtpRequest.dart';
import '../Model/change_password/ChangePasswordRequest.dart';
import '../Model/social/LoginMediaRequest.dart';
import '../Model/social/SocialMediaRequest.dart';

abstract class AuthAPI {
  Future<dynamic> loginUser({
    @required LoginRequest requestBody,
    @required BuildContext context,
  });

  Future<dynamic> loginSocialUser({
    @required LoginMediaRequest requestBody,
    @required BuildContext context,
  });

  Future<dynamic> signSocialUser({
    @required SocialMediaRequest requestBody,
    @required BuildContext context,
  });

  Future<dynamic> signUpUser({
    @required SignUpRequest requestBody,
    @required BuildContext context,
  });

  Future<dynamic> forgotPasswordUser({
    @required ForgotPasswordRequest requestBody,
    @required BuildContext context,
  });

  Future<dynamic> resendforgotPasswordUser({
    @required ForgotPasswordRequest requestBody,
    @required BuildContext context,
  });

  Future<dynamic> resendsignupPasswordUser({
    @required ForgotPasswordRequest requestBody,
    @required BuildContext context,
  });

  Future<dynamic> forgotOTPVerifyUser({
    @required ForgotPasswordOtpRequest requestBody,
    @required BuildContext context,
  });

  Future<dynamic> resendOTPUser({
    @required Map requestBody,
    @required BuildContext context,
  });

  Future<dynamic> resetPasswordUser({
    @required Map requestBody,
    @required BuildContext context,
  });

  Future<dynamic> changePasswordUser({
    @required ChangePasswordRequest requestBody,
    @required BuildContext context,
  });

  Future<dynamic> changePhoneEmail({
    @required Map requestBody,
    @required BuildContext context,
  });

  Future<dynamic> signUpOTPVerifyUser({
    @required RegistrationOtpRequest requestBody,
    @required BuildContext context,
  });

  Future<dynamic> changePhoneEmailOTPVerifyUser({
    @required Map requestBody,
    @required BuildContext context,
  });
}
