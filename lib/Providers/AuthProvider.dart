import 'package:flutter/widgets.dart';

import '../BLocs/AuthBLoc.dart';
import '../Network/AuthServiceAPI.dart';

class AuthProvider extends InheritedWidget {
  final AuthBLoc authBLoc;

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  AuthProvider({
    Key key,
    AuthBLoc authBloc,
    Widget child,
  })  : authBLoc = authBloc ?? AuthBLoc(authAPI: new AuthServiceAPI()),
        super(key: key, child: child);

  static AuthBLoc of(BuildContext context) =>
      (context.inheritFromWidgetOfExactType(AuthProvider) as AuthProvider)
          .authBLoc;
}
