class LoginRequest {
  String username;
  String password;

  LoginRequest({this.username, this.password});

  LoginRequest.fromJson(Map<String, String> json) {
    username = json['username'];
    password = json['password'];
  }

  Map<String, String> toJson() {
    final Map<String, String> data = new Map<String, String>();
    data['username'] = this.username;
    data['password'] = this.password;
    print("new $data");
    return data;
  }
}
