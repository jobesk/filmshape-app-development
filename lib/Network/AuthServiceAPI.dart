import 'dart:async';

import 'package:flutter/material.dart';

import '../AbstractApi/AuthAPI.dart';
import '../Model/ForgotPassword/ForgotPasswordRequest.dart';
import '../Model/ForgotPassword/ForgotPasswordResponse.dart';
import '../Model/ForgotPassword/otp_verification/ForgotPasswordOtpRequest.dart';
import '../Model/Login/LoginRequest.dart';
import '../Model/Login/LoginResponse.dart';
import '../Model/Network/APIError.dart';
import '../Model/ResendOTP/ResendOTPResponse.dart';
import '../Model/SignUp/SignUpRequest.dart';
import '../Model/SignUp/SignupResponse.dart';
import '../Model/SignUp/registration_otp/RegistrationOtpRequest.dart';
import '../Model/SignUp/registration_otp/RegistrationOtpResponse.dart';
import '../Model/change_password/ChangePasswordRequest.dart';
import '../Model/change_password/ChangePasswordResponse.dart';
import '../Model/social/LoginMediaRequest.dart';
import '../Model/social/SocialMediaRequest.dart';
import '../Utils/APIs.dart';
import 'APIHandler.dart';

class AuthServiceAPI extends AuthAPI {
  @override
  // Hits Login api
  Future<dynamic> loginUser({
    @required BuildContext context,
    @required LoginRequest requestBody,
  }) async {
    Completer<dynamic> completer = new Completer<dynamic>();
    var response = await APIHandler.post(
      context: context,
      requestBody: requestBody,
      url: APIs.loginUrl,
    );
    if (response is APIError) {
      completer.complete(response);
      return completer.future;
    } else {
      LoginResponse loginResponse = new LoginResponse.fromJson(response);
      completer.complete(loginResponse);
      return completer.future;
    }
  }

  Future<dynamic> loginSocialUser({
    @required BuildContext context,
    @required LoginMediaRequest requestBody,
  }) async {
    Completer<dynamic> completer = new Completer<dynamic>();
    var response = await APIHandler.post(
      context: context,
      requestBody: requestBody,
      url: APIs.loginUrl, //not declared
    );
    if (response is APIError) {
      completer.complete(response);
      return completer.future;
    } else {
      LoginResponse loginResponse = new LoginResponse.fromJson(response);
      completer.complete(loginResponse);
      return completer.future;
    }
  }

  Future<dynamic> signSocialUser({
    @required BuildContext context,
    @required SocialMediaRequest requestBody,
  }) async {
    Completer<dynamic> completer = new Completer<dynamic>();
    var response = await APIHandler.post(
      context: context,
      requestBody: requestBody,
      url: APIs.signUpUrl, //not declared
    );
    if (response is APIError) {
      completer.complete(response);
      return completer.future;
    } else {
      LoginResponse loginResponse = new LoginResponse.fromJson(response);
      completer.complete(loginResponse);
      return completer.future;
    }
  }

  // Hits signup user api
  Future<dynamic> signUpUser({
    @required BuildContext context,
    @required SignUpRequest requestBody,
  }) async {
    Completer<dynamic> completer = new Completer<dynamic>();

    var response = await APIHandler.post(
      context: context,
      requestBody: requestBody,
      url: APIs.signUpUrl,
    );

    if (response is APIError) {
      completer.complete(response);
      return completer.future;
    } else {
      SignupResponse loginResponse = new SignupResponse.fromJson(response);
      completer.complete(loginResponse);
      return completer.future;
    }
  }

  // Hits signup otp verify Otp api
  Future<dynamic> signUpOTPVerifyUser({
    @required BuildContext context,
    @required RegistrationOtpRequest requestBody,
  }) async {
    Completer<dynamic> completer = new Completer<dynamic>();

    var response = await APIHandler.post(
      context: context,
      requestBody: requestBody,
      url: APIs.signUpUrl, //not found
    );

    if (response is APIError) {
      completer.complete(response);
      return completer.future;
    } else {
      RegistrationOtpResponse loginResponse =
          new RegistrationOtpResponse.fromJson(response);
      completer.complete(loginResponse);
      return completer.future;
    }
  }

  // Hits change phone email otp verify Otp api
  Future<dynamic> changePhoneEmailOTPVerifyUser({
    @required BuildContext context,
    @required Map requestBody,
  }) async {
    Completer<dynamic> completer = new Completer<dynamic>();
    var response = await APIHandler.put(
      context: context,
      requestBody: requestBody,
      url: APIs.changePassword, //not found
    );

    if (response is APIError) {
      completer.complete(response);
      return completer.future;
    } else {
      completer.complete(response);
      return completer.future;
    }
  }

  // Hits forgotPassword api
  Future<dynamic> forgotPasswordUser({
    @required BuildContext context,
    @required ForgotPasswordRequest requestBody,
  }) async {
    Completer<dynamic> completer = new Completer<dynamic>();
    var response = await APIHandler.post(
      context: context,
      requestBody: requestBody,
      url: APIs.forgotUrl, //not found //older one is forgotpasswordUrl
    );

    if (response is APIError) {
      completer.complete(response);
      return completer.future;
    } else {
      ForgotPasswordResponse forgotPasswordResponse =
          new ForgotPasswordResponse.fromJson(response);
      completer.complete(forgotPasswordResponse);
      return completer.future;
    }
  }

  // Hits forgotPassword api
  Future<dynamic> resendforgotPasswordUser({
    @required BuildContext context,
    @required ForgotPasswordRequest requestBody,
  }) async {
    Completer<dynamic> completer = new Completer<dynamic>();
    var response = await APIHandler.post(
      context: context,
      requestBody: requestBody,
      url: APIs.forgotUrl, //not found
    );

    if (response is APIError) {
      completer.complete(response);
      return completer.future;
    } else {
      ForgotPasswordResponse forgotPasswordResponse =
          new ForgotPasswordResponse.fromJson(response);
      completer.complete(forgotPasswordResponse);
      return completer.future;
    }
  }

  // Hits forgotPassword api
  Future<dynamic> resendsignupPasswordUser({
    @required BuildContext context,
    @required ForgotPasswordRequest requestBody,
  }) async {
    Completer<dynamic> completer = new Completer<dynamic>();
    var response = await APIHandler.post(
      context: context,
      requestBody: requestBody,
      url: APIs.signUpUrl, //not found
    );

    if (response is APIError) {
      completer.complete(response);
      return completer.future;
    } else {
      ForgotPasswordResponse forgotPasswordResponse =
          new ForgotPasswordResponse.fromJson(response);
      completer.complete(forgotPasswordResponse);
      return completer.future;
    }
  }

  // Hits forgotPassword Otp verify api
  Future<dynamic> forgotOTPVerifyUser({
    @required BuildContext context,
    @required ForgotPasswordOtpRequest requestBody,
  }) async {
    Completer<dynamic> completer = new Completer<dynamic>();

    var response = await APIHandler.post(
      context: context,
      requestBody: requestBody,
      url: APIs.forgotUrl, //not found
    );

    if (response is APIError) {
      completer.complete(response);
      return completer.future;
    } else {}
  }

  // Hits forgotPassword Otp verify api
  Future<dynamic> resetPasswordUser({
    @required BuildContext context,
    @required Map requestBody,
  }) async {
    Completer<dynamic> completer = new Completer<dynamic>();
    var response = await APIHandler.put(
      context: context,
      requestBody: requestBody,
      url: APIs.forgotUrl, //notfound
    );

    if (response is APIError) {
      completer.complete(response);
      return completer.future;
    } else {
      ChangePasswordResponse changePasswordResponse =
          new ChangePasswordResponse.fromJson(response);
      completer.complete(changePasswordResponse);
      return completer.future;
    }
  }

  // Hits change Password api
  Future<dynamic> changePasswordUser({
    @required BuildContext context,
    @required ChangePasswordRequest requestBody,
  }) async {
    Completer<dynamic> completer = new Completer<dynamic>();

    var response = await APIHandler.post(
      context: context,
      requestBody: requestBody,
      url: APIs.changePassword, //not found
    );

    if (response is APIError) {
      completer.complete(response);
      return completer.future;
    } else {
      ChangePasswordResponse changePasswordResponse =
          new ChangePasswordResponse.fromJson(response);
      completer.complete(changePasswordResponse);
      return completer.future;
    }
  }

  // Hits change phone-email api
  Future<dynamic> changePhoneEmail({
    @required BuildContext context,
    @required Map requestBody,
  }) async {
    Completer<dynamic> completer = new Completer<dynamic>();

    var response = await APIHandler.post(
      context: context,
      requestBody: requestBody,
      url: APIs.changePassword, //notfound
    );

    if (response is APIError) {
      completer.complete(response);
      return completer.future;
    } else {
      completer.complete(response);
      return completer.future;
    }
  }

  // Hits resend Otp api
  Future<dynamic> resendOTPUser({
    @required BuildContext context,
    @required Map requestBody,
  }) async {
    Completer<dynamic> completer = new Completer<dynamic>();

    var response = await APIHandler.post(
      context: context,
      requestBody: requestBody,
      url: APIs.sendinvite, //notfound
    );

    if (response is APIError) {
      completer.complete(response);
      return completer.future;
    } else {
      ResendOTPResponse resendOtpResponse =
          new ResendOTPResponse.fromJson(response);
      completer.complete(resendOtpResponse);
      return completer.future;
    }
  }
}
