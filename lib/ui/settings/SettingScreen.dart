import 'package:Filmshape/Utils/AppColors.dart';
import 'package:Filmshape/Utils/AssetStrings.dart';
import 'package:Filmshape/Utils/Constants/AppCustomTheme.dart';
import 'package:Filmshape/Utils/Constants/Const.dart';
import 'package:Filmshape/Utils/ReusableWidgets.dart';
import 'package:Filmshape/Utils/UniversalFunctions.dart';
import 'package:Filmshape/Utils/memory_management.dart';
import 'package:Filmshape/ui/payment_screen/PaymentScreen.dart';
import 'package:Filmshape/ui/settings/AccountsAndSecurityScreen.dart';
import 'package:Filmshape/ui/settings/ContactSupportScreen.dart';
import 'package:Filmshape/ui/settings/PushNotificationScreen.dart';
import 'package:Filmshape/ui/settings/ReportViolationScreen.dart';
import 'package:Filmshape/ui/settings/settings_connect_account.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:notifier/main_notifier.dart';
import 'package:notifier/notifier_provider.dart';
import 'package:package_info/package_info.dart';
import 'package:url_launcher/url_launcher.dart';

class SettingScreen extends StatefulWidget {
  final ValueChanged<Widget> fullScreenCallBack;
  String previousTabHeading;

  SettingScreen(this.fullScreenCallBack, {this.previousTabHeading});

  @override
  _SettingScreenState createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {
  Notifier _notifier;
  String version;

  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(milliseconds: 100), () {
      _notifier?.notify('action', "Settings"); //update title
      getVersionDetails();
    });
  }

  void getVersionDetails() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String appName = packageInfo.appName;
    String packageName = packageInfo.packageName;
    version = packageInfo.version;
    String buildNumber = packageInfo.buildNumber;
    setState(() {});
  }

  @override
  void dispose() {
    //update previous heading
    if (widget.previousTabHeading != null)
      _notifier?.notify('action', widget.previousTabHeading);

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _notifier = NotifierProvider.of(context); // to update home screen header

    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new SizedBox(
                  height: 30.0,
                ),
                Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30),
                    child: SettingHeader(
                        text: "Account",
                        iconPath: AssetStrings.account_person)),
                SizedBox(
                  height: 15.0,
                ),
                InkWell(
                    onTap: () {
                      widget
                          .fullScreenCallBack(new AccountsANdSecurityScreen());
                    },
                    child: _getTextView("Account and Security")),
                _getView(),
                InkWell(
                    onTap: () {
                      //if current user is not pro user
                      if (!isProUser()) {
                        widget.fullScreenCallBack(PaymentScreen());
                      } else {
                        widget.fullScreenCallBack(ProScreen());
                      }
                    },
                    child: Notifier.of(context)
                        .register<String>(ACCOUNT_UPGRADED_NOTIFIER, (data) {
                      return getTwoSidedTxtView(
                          "Membership", isProUser() ? "Pro" : "Free");
                    })),
                _getView(),
                InkWell(
                    onTap: () {
                      widget.fullScreenCallBack(new PushNotificationScreen());
                    },
                    child: _getTextView("Push notifications")),
                _getView(),
                InkWell(
                    onTap: () {
                      widget.fullScreenCallBack(new SettingsConnectAccount());
                    },
                    child: _getTextView("Connected accounts")),
                _getView(),
                SizedBox(
                  height: 20.0,
                ),
                Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30),
                    child: SettingHeader(
                        text: "Support", iconPath: AssetStrings.liveHelp)),
                SizedBox(
                  height: 15.0,
                ),
                InkWell(
                    onTap: () {
                      widget.fullScreenCallBack(new ContactSupportScreen());
                    },
                    child: _getTextView("Contact Support")),
                _getView(),
                InkWell(
                    onTap: () {
                      widget.fullScreenCallBack(new ReportViolationScreen());
                    },
                    child: _getTextView("Report a violation")),
                _getView(),
                SizedBox(
                  height: 20.0,
                ),
                Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30),
                    child: SettingHeader(
                        text: "Terms and policies",
                        iconPath: AssetStrings.files)),
                SizedBox(
                  height: 15.0,
                ),
                InkWell(
                    onTap: _launchURL,
                    child: _getTextView("Terms and conditions")),
                _getView(),
                InkWell(
                    onTap: _launchURL, child: _getTextView("Privacy policy")),
                _getView(),
                InkWell(
                    onTap: _launchURL, child: _getTextView("Cookie policy")),
                _getView(),
                _getTextView("Disclaimer"),
                _getView(),
                SizedBox(
                  height: 10.0,
                ),
                getTwoSidedTxtView("App version", version ?? "0.0"),
                SizedBox(
                  height: 40.0,
                ),
              ]),
        ),
      ),
    );
  }

  Widget _getTextView(String txt) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: 18.0,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30),
          child: Text(
            txt,
            style: TextStyle(
                color: AppColors.settingBlackText,
                fontSize: 16.0,
                fontFamily: AssetStrings.lotoRegularStyle),
          ),
        ),
        SizedBox(
          height: 18.0,
        ),
      ],
    );
  }

  Widget getTwoSidedTxtView(String txt1, String txt2) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: 18.0,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30),
          child: Row(
            //  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Expanded(
                child: Text(
                  txt1,
                  style: TextStyle(
                      color: AppColors.settingBlackText,
                      fontSize: 16.0,
                      fontFamily: AssetStrings.lotoRegularStyle),
                ),
              ),
              Spacer(),
              Text(
                txt2,
                style: TextStyle(
                    color: AppColors.settingBlackText,
                    fontSize: 16.0,
                    fontFamily: AssetStrings.lotoRegularStyle),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 18.0,
        ),
      ],
    );
  }

  Widget _getView() {
    return new Container(
      height: 1.0,
      color: AppColors.dividerColorsSettings,
    );
  }

  _launchURL() async {
    const url = 'https://flutter.dev';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}

class SettingHeader extends StatelessWidget {
  final String text;
  final String iconPath;

  SettingHeader({@required this.text, @required this.iconPath});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        new SvgPicture.asset(
          iconPath,
          width: 24.0,
          height: 24.0,
          fit: BoxFit.cover,
          color: AppColors.payment_background,
        ),
        new SizedBox(
          width: 10.0,
        ),
        new Text(
          text,
          style: new TextStyle(
              color: Colors.black,
              fontFamily: AssetStrings.lotoRegularStyle,
              fontSize: 20.0),
        ),
      ],
    );
  }
}

class ProScreen extends StatefulWidget {
  final ValueChanged<bool> callback;

  ProScreen({this.callback});

  @override
  _ProScreenState createState() => _ProScreenState();
}

class _ProScreenState extends State<ProScreen> {
  Notifier _notifier;

  Widget getItem(String icon, String text) {
    return Container(
      margin: new EdgeInsets.only(left: 25.0, top: 25.0),
      alignment: Alignment.center,
      child: new Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new SvgPicture.asset(
            icon,
            color: AppColors.kPrimaryBlue,
            width: 20.0,
            height: 20.0,
          ),
          new SizedBox(
            width: 15.0,
          ),
          Expanded(
            child: new Text(
              text,
              style: new TextStyle(
                  fontFamily: AssetStrings.lotoRegularStyle, fontSize: 16.0),
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    _notifier = NotifierProvider.of(context); // to update home screen header
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: Container(
            child: new Stack(
              children: <Widget>[
                // Positioned(
                //   right: -35.0,
                //   top: -35.0,
                //   child: Container(
                //     alignment: Alignment.topRight,
                //     child: new SvgPicture.asset(
                //       AssetStrings.payment_image_top_new,
                //       width: 80,
                //       height: 80.0,
                //     ),
                //   ),
                // ),
                new Column(
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        margin: new EdgeInsets.only(left: 25.0, top: 20.0),
                        child: new Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Icon(
                              Icons.arrow_back,
                            ),
                            new SizedBox(
                              width: 3.0,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 4),
                              child: new Text(
                                "Back",
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Divider(),
                    Padding(
                      padding: const EdgeInsets.only(left: 10.0),
                      child: Row(
                        children: [
                          Text(
                            "Membership",
                            textAlign: TextAlign.start,
                            style: new TextStyle(
                                fontFamily: AssetStrings.lotoBoldStyle,
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 20),
                          ),
                        ],
                      ),
                    ),
                    new SizedBox(height: 7),
                    Padding(
                      padding: const EdgeInsets.all(13.0),
                      child: new Text(
                        "You Filmshape Pro Membership will renew automatically on [renew data]. Your next charge will be 3 Euros",
                        style: new TextStyle(
                            fontFamily: AssetStrings.lotoBoldStyle,
                            color: AppColors.kGrey,
                            fontSize: 14),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.grey[300],
                            borderRadius: BorderRadius.circular(2)),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            "Thank You for being a valuable member of our community.♥",
                            style: TextStyle(color: AppColors.kPrimaryBlue),
                          ),
                        ),
                      ),
                    ),

                    /*   Container(
                          width: 100,
                          height: 100,
                          child: new Image.asset(AssetStrings.propayment)),
*/

                    new SizedBox(
                      height: 10.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(13.0),
                      child: InkWell(
                        onTap: () {},
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: AppColors.grey,
                                  style: BorderStyle.solid)),
                          padding: EdgeInsets.symmetric(
                              vertical: 10, horizontal: 15),
                          child: Center(
                            child: Text(
                              "Cancel Pro Membership",
                              style: TextStyle(fontSize: 20),
                            ),
                          ),
                        ),
                      ),
                    ),
                    // getContinueProfileSetupDarkBlueButton(callback, "Upgrade"),
                    new SizedBox(
                      height: 20.0,
                    ),
                    Divider(
                      height: 3,
                      color: AppColors.grey,
                    ),
                    new SizedBox(
                      height: 10.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(13.0),
                      child: Wrap(
                        children: [
                          Text(
                            "After Your First Payment your Filmshape Pro membership will automatically renew at same rate untill you cancel. You are authorizing us to charge your card untill you next billing date shown you above.",
                            style: TextStyle(color: AppColors.kGrey),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  VoidCallback callback() {
    MemoryManagement.setUpgradedAccount(true); //mark account purchased
    _notifier?.notify(ACCOUNT_UPGRADED_NOTIFIER, "pro"); //update heading
    if (widget.callback != null) {
      widget.callback(true);
    }
    Navigator.pop(context);
  }
}
