import 'package:flutter/cupertino.dart';
import 'package:flutter_html/flutter_html.dart';

class HtmlTextWidget extends StatelessWidget {
  String message;
  HtmlTextWidget(this.message);
  @override
  Widget build(BuildContext context) {
    return Html(
      shrinkToFit: false,
      data: "$message",
      defaultTextStyle: TextStyle(
        fontSize: 16,
        color: Color(0xFF222B47), //changed
      ),
    );
  }
}
